import logging
from datetime import datetime

from werkzeug.middleware.proxy_fix import ProxyFix

from app.web_games import startup


(app, socketio_app) = startup()
logging.info(f'Started at {datetime.now()}')
app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1)  # accept proxy headers
socketio_app.run(app, host='0.0.0.0', port=5000)
