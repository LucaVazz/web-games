from app.web_games import startup


(app, socketio_app) = startup()
socketio_app.run(app, ssl_context=('ssl.cert', 'ssl.key'))
