from flask_mongoengine import Document
from mongoengine import StringField, URLField


class User (Document):
    discord_id = StringField(required=True, unique=True, min_length=7, max_length=20)
    name = StringField(required=True)
    avatar_url = URLField(required=True)
    color = StringField(required=True, regex=r'^\#[0-9a-fA-F]{6}$')
    locale = StringField(required=True)
