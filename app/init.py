import logging

from flask import Flask, send_from_directory, request, render_template, session
from flask_babel import Babel
from flask_mongoengine import MongoEngine
from flask_socketio import SocketIO

import app._version
from .settings.settings_resolver import Settings
from .util.common_constants import SESSION_DISCORD_ID_KEY
from .util.error.user_not_found_error import UserNotFoundError
from .util.locale import get_locale_from_request
from .util.user_guard import get_user_from_session


def configure_logging():
    logging.basicConfig(format='%(asctime)s | %(name)s | %(levelname)s: %(message)s', level=logging.INFO)
    logging.getLogger('werkzeug').setLevel(logging.WARNING)
    logging.getLogger('engineio').setLevel(logging.WARNING)

    logging.info('Starting, setting up config...')


def configure_sentry(settings: Settings):
    try:
        import sentry_sdk
        from sentry_sdk.integrations.flask import FlaskIntegration
        sentry_sdk.init(
            dsn=settings.get('sentry_dsn'),
            integrations=[FlaskIntegration()],
            release=app._version.__version__
        )

        logging.info('configured Sentry')
    except ModuleNotFoundError:
        logging.warning("couldn't configure Sentry - sentry_sdk not installed")
    except BaseException as ex:
        logging.warning(f"couldn't configure Sentry - other error: {str(ex)}")


def configure_app(settings: Settings) -> Flask:
    app = Flask(__name__, static_folder='static')
    app.secret_key = settings.get('secret_key')

    app.add_url_rule('/favicon.ico', view_func=lambda: send_from_directory('static', filename='media/favicon.ico'))
    return app


def configure_db(app: Flask, settings: Settings) -> MongoEngine:
    app.config['MONGODB_SETTINGS'] = settings.get('mongo')
    db = MongoEngine(app)
    return db


def configure_i18n(app: Flask):
    babel = Babel(app)

    def get_locale():
        try:
            return get_user_from_session().locale
        except UserNotFoundError:
            return get_locale_from_request()
    babel.localeselector(get_locale)


def configure_request_logging(app: Flask):
    server_log = logging.getLogger('server')

    def log_request_info():
        if 'health' in request.full_path or 'static' in request.full_path:
            return

        discord_id = session.get(SESSION_DISCORD_ID_KEY, "n/a")

        if request.method == 'POST':
            server_log.info(
                f'got request - url={request.full_path} method=POST session.discord_id={discord_id} data={request.form}'
            )
        else:
            server_log.info(
                f'got request - url={request.full_path} method={request.method} session.discord_id={discord_id}'
            )
    app.before_request(log_request_info)


def configure_bots_redirect(app: Flask):
    def redirect_bots():
        user_agent = request.headers.get('User-Agent')
        if user_agent is None or 'bot' in user_agent.lower():
            return render_template('bot.jinja2')
    app.before_request(redirect_bots)


def configure_socketio_app(app: Flask, settings: Settings) -> SocketIO:
    socketio_app = SocketIO(
        app, ping_timeout=settings.get('ws_ping.timeout'), ping_interval=settings.get('ws_ping.interval')
    )

    def error_ws(e):
        try:
            from sentry_sdk import capture_exception
            capture_exception(e)
            logging.warning(f'logged error from WS handling to sentry, was: {str(e)}')

        except ModuleNotFoundError:
            logging.error('error during WS handling:')
            logging.exception(e)

    socketio_app.default_exception_handler = error_ws

    return socketio_app
