class DeckInfo:
    def __init__(self, file_name: str, name: str, is_original: bool):
        self.file_name = file_name
        self.name = name
        self.is_original = is_original


class Deck:
    def __init__(self, black_cards: [str], white_cards: [str]):
        self.black_cards = black_cards
        self.white_cards = white_cards
