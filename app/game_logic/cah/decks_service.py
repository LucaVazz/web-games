import os

import yaml

from .model.deck import Deck, DeckInfo
from .model.card import Card

path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'decks')


def list_decks() -> [DeckInfo]:
    files = [
        file_name
        for dir_path, dir_names, file_names in os.walk(path)
        for file_name in file_names if os.path.splitext(file_name)[1] == '.yaml'
    ]

    decks_list = []
    for file_name in files:
        file_path = os.path.join(os.path.dirname(__file__), 'decks', f'{file_name}')
        try:
            with open(file_path) as f:
                data = yaml.safe_load(f)
            name = data.get('name')
            if name is None:
                raise RuntimeError(f'no name specified for `{file_name}`')
            is_original = data.get('original', False)

            file_path = file_name[:-len('.yaml')]
            decks_list.append(DeckInfo(file_path, name, is_original))
        except BaseException:
            raise RuntimeError(f'Failed when loading `{file_name}`')

    return sorted(decks_list, key=lambda deck: deck.name)


def find_and_load_deck(file_name: str) -> Deck:
    file_path = os.path.join(os.path.dirname(__file__), 'decks', f'{file_name}.yaml')
    with open(file_path, 'r') as f:
        data = yaml.safe_load(f)

    black_cards = [Card.generate(text) for text in data.get('questions', []) if text.count('_') <= 1]
    white_cards = [Card.generate(text) for text in data.get('answers', [])]

    return Deck(black_cards, white_cards)
