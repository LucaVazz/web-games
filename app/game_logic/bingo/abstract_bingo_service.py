from abc import abstractmethod
from typing import Optional


class AbstractBingoService:
    @classmethod
    @abstractmethod
    def get_card_size(cls) -> (int, int):
        """
        Get the card size for the specific bingo variant.
        :return: A tuple row_length, row_count
        """
        pass

    @classmethod
    @abstractmethod
    def get_max_number(cls) -> int:
        """
        Get the highest possible number that can be called out
        """
        pass

    @classmethod
    @abstractmethod
    def generate_card(cls) -> [[Optional[int]]]:
        """
        Generates a random card for the specific bingo variant.
        :return: A two dimensional array representing the card
        """
        pass

    @classmethod
    @abstractmethod
    def check_win(cls, card: [[Optional[int]]], marked: [[bool]]) -> bool:
        """
        Checks for a given card if it is marked in a way that counts as a win.
        :param card: A two dimensional array representing the numbers on the card
        :param marked: A two dimensional array, representing what fields are marked already
        :return: If its a win
        """
        pass
