from mongoengine import EmbeddedDocument, IntField, StringField


class Number(EmbeddedDocument):
    number = IntField(required=True, min_value=1)
    description = StringField(required=True)
    explanation = StringField(required=True)
