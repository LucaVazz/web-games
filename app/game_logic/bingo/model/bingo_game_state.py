from enum import Enum


class BingoGameState(Enum):
    STARTING = 'STARTING'
    PLAYING = 'PLAYING'
    ROUND_DONE = 'ROUND_DONE'
    # we don't use FINISHED as we don't want to display the results screen
