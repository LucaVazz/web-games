from enum import Enum


class ElectGameState(Enum):
    STARTING = 'STARTING'
    VOTING = 'VOTING'
    RESULT_SUMMARY = 'RESULT_SUMMARY'
    RESULT_DETAILS = 'RESULT_DETAILS'
    FINISHED = 'FINISHED'
