from flask_babel import _
from werkzeug.datastructures import ImmutableMultiDict

from app.game_logic.base.base_game import BaseGame
from app.model.user import User
from .elect_match import ElectMatch
from ...util.error.bad_user_request_error import BadUserRequestError


class ElectGame(BaseGame):
    @classmethod
    def get_internal_name(cls) -> str:
        return "elect"

    @classmethod
    def get_long_name(cls) -> str:
        return _("elect_name")

    @classmethod
    def render_view_new(cls, user: User):
        return cls._render_game_template('new', user=user)

    @classmethod
    def init_new_match(cls, data: ImmutableMultiDict, initiator: User):
        try:
            points_to_win = int(data.get('pointsToWin', -1))
            if points_to_win < 1:
                raise ValueError()
        except ValueError:
            raise BadUserRequestError(_('invalid_winning_point_count'))

        return ElectMatch.init_new_match(
            initiator,
            points_to_win=points_to_win
        )
