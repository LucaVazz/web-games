import random
from typing import Union

from mongoengine import EmbeddedDocumentListField, EmbeddedDocumentField, ReferenceField, ListField, BooleanField
from werkzeug.datastructures import ImmutableMultiDict

from app.game_logic.sos.model.sos_player_state import SosPlayerState
from app.model.user import User
from app.util.error.bad_user_request_error import BadUserRequestError
from app.util.mongoengine_fields import StringEnumField
from .model.card import Card
from .model.sos_game_state import SosGameState
from ..base.base_match import BaseMatch
from ..base.model.base_player_state import BasePlayerState


class SosMatch(BaseMatch):

    # region Fields
    game_state = StringEnumField(SosGameState, required=True)
    player_states = ListField(ReferenceField('SosPlayerState'))
    is_simple_mode = BooleanField(required=True)

    cards_stack = EmbeddedDocumentListField(Card)
    """
    The cards that are yet to be drawn
    """
    current_card = EmbeddedDocumentField(Card)

    lead = ReferenceField(User)
    target = ReferenceField(User)
    # endregion

    # region Class Methods
    @classmethod
    def get_internal_name(cls) -> str:
        return 'sos'
    # endregion

    # region Constructor
    def _gl_init(self, **kwargs) -> None:
        self.is_simple_mode = True

        cards_stack = kwargs['cards']
        if self.is_simple_mode:
            spread_cards = []
            for card in cards_stack:
                if card.round_on_me:
                    spread_cards.append(card)
                else:
                    spread_cards.append(Card(round_on_me=False, text_a=card.text_a, colour=card.colour))
                    spread_cards.append(Card(round_on_me=False, text_a=card.text_b, colour=card.colour))
            cards_stack = spread_cards

        random.shuffle(cards_stack)
        self.cards_stack = cards_stack

        self.game_state = SosGameState.STARTING
    # endregion

    # region Event Handlers
    def _gl_handle_join_init_player_state(self, user: User) -> SosPlayerState:
        return SosPlayerState(asked_count=0)

    def _gl_handle_join(self, user: User) -> None:
        pass

    def _gl_handle_disconnect(self, user: User) -> None:
        if user == self.lead:
            self._advance_round_number()
            self.__start_new_round()

    def _gl_handle_rejoin(self, user: User, player_state: BasePlayerState) -> None:
        pass

    def _gl_handle_play(self, user: User, data: ImmutableMultiDict) -> None:
        action = data.get('action')
        if action is None:
            raise BadUserRequestError('no action')

        is_lead = user == self.lead

        if action == 'start':
            if self.game_state != SosGameState.STARTING:
                raise BadUserRequestError('invalid action - cannot start when not in STARTING state')

            self.__start_new_round()

        elif action == 'target_selected':
            if self.game_state != SosGameState.SELECTING_TARGET:
                raise BadUserRequestError('invalid action - cannot select target when not in SELECTING_TARGET state')
            if not is_lead:
                raise BadUserRequestError('invalid action - non-lead cannot select target')

            try:
                index = int(data.get('index'))
            except BaseException as ex:
                raise BadUserRequestError(f'invalid action - invalid index passed ({str(ex)})')
            target_player = self.players[index]

            self.modify(set__target=target_player)
            self.get_player_state(target_player).modify(inc__asked_count=1)

            self.modify(set__game_state=SosGameState.ANSWERING_ONE)
            self._emit_ws_state_change()

        elif action == 'answered':
            if self.game_state not in [SosGameState.ANSWERING_ONE, SosGameState.ANSWERING_ALL]:
                raise BadUserRequestError('invalid action - cannot set as answered when not in ANSWERING_* state')
            if not is_lead:
                raise BadUserRequestError('invalid action - non-lead cannot set as answered')

            self._advance_round_number()
            self.__start_new_round()

        else:
            raise BadUserRequestError(f'bad action `{action}`')

    def __start_new_round(self):
        if len(self.cards_stack) == 0:
            self.modify(set__game_state=SosGameState.DONE)
            self._emit_ws_state_change()
            return

        if self.target is None:
            new_lead = random.choice(self.players)
        else:
            new_lead = self.target
        self.modify(set__lead=new_lead)
        self.modify(set__target=None)

        new_card = self.cards_stack[0]
        self.modify(set__current_card=new_card)
        self.modify(pull__cards_stack=new_card)

        if new_card.round_on_me:
            self.modify(set__game_state=SosGameState.ANSWERING_ALL)
            self.get_player_state(new_lead).modify(inc__asked_count=1)
        else:
            self.modify(set__game_state=SosGameState.SELECTING_TARGET)

        self._emit_ws_state_change()
    # endregion

    # region View Renderers
    def _gl_render_inner_view_for_state(self, user: User, player_state: BasePlayerState = None) -> Union[str, dict]:
        is_lead = user == self.lead

        if self.game_state == SosGameState.STARTING:
            user_is_initiator = user == self.initiator
            return self._render_base_state_template(
                'starting', user, user_is_initiator=user_is_initiator
            )

        elif self.game_state == SosGameState.SELECTING_TARGET:
            if not is_lead:
                return self._render_state_template(
                    'target_waiting', user, lead=self.lead
                )

            else:
                possible_targets = [
                    {'player': p, 'index': i}
                    for i, p in enumerate(self.players)
                    if p != self.lead
                ]

                max_asked_count = (self.current_round_number / len(self.players)) * 1.5
                allowed_targets = [
                    t
                    for t in possible_targets
                    if self.get_player_state(t.get('player')).asked_count < max_asked_count
                ]
                if len(allowed_targets) == 0:
                    allowed_targets = possible_targets

                return self._render_state_template(
                    'target_selecting', user, card=self.current_card, targets=allowed_targets
                )

        elif self.game_state == SosGameState.ANSWERING_ONE:
            return self._render_state_template(
                'answering', user, card=self.current_card,
                lead=self.lead, target=self.target, is_lead=is_lead, user_is_target=(user == self.target)
            )

        elif self.game_state == SosGameState.ANSWERING_ALL:
            return self._render_state_template(
                'answering', user, card=self.current_card,
                lead=self.lead, target=None, is_lead=is_lead
            )

        elif self.game_state == SosGameState.DONE:
            return self._render_state_template(
                'done', user
            )

        else:
            raise RuntimeError(f'cannot render state `{self.game_state}`')

    def _gl_get_player_tags(self, user: User) -> [str]:
        return []
    # endregion
