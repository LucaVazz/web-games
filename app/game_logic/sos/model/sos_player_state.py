from mongoengine import IntField

from app.game_logic.base.model.base_player_state import BasePlayerState


class SosPlayerState(BasePlayerState):
    asked_count = IntField(required=True)
