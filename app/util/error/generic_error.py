class GenericError(Exception):
    def __init__(self, msg: str, details: str = None):
        self.msg = msg
        self.details = details
