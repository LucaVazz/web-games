from .generic_error import GenericError


class UserNotFoundError(GenericError):
    def __init__(self, details: str = None):
        self.msg = 'No user found in current session'
        self.details = details
